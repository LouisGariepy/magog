use iced::button::Style;
use iced::{button, container, Background, Color};

pub struct Container;

impl container::StyleSheet for Container {
    fn style(&self) -> container::Style {
        container::Style {
            border_radius: 0.0,
            border_width: 2.0,
            border_color: Color::BLACK,
            ..Default::default()
        }
    }
}

pub struct Button {
    pub is_selected: bool,
}

pub struct RemoveButton;

impl button::StyleSheet for RemoveButton {
    fn active(&self) -> Style {
        button::Style {
            background: Some(Background::Color(Color::from_rgb(0.7, 0.1, 0.1))),
            border_radius: 5.0,
            text_color: Color::WHITE,
            ..button::Style::default()
        }
    }
}

impl button::StyleSheet for Button {
    fn active(&self) -> button::Style {
        if self.is_selected {
            button::Style {
                background: Some(Background::Color(Color::from_rgb(0.2, 0.2, 0.7))),
                border_radius: 5.0,
                text_color: Color::WHITE,
                ..button::Style::default()
            }
        } else {
            button::Style::default()
        }
    }
}
