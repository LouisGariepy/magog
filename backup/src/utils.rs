use crate::item_catalog_header::{HeapSize, HousingType};
use crate::item_sidebar::ItemCondition;
use crate::start_screen::Sector;
use crate::street_searchbar::StreetStatus;
use chrono::{DateTime, Local};
use iced::button;
use indexmap::map::{Values, ValuesMut};
use indexmap::IndexMap;
use serde::ser::{SerializeMap, SerializeSeq, SerializeStruct};
use serde::{Serialize, Serializer};
use std::error::Error;
use std::fmt::Display;
use std::time::Instant;
use strsim::jaro_winkler;

pub struct ButtonInfo {
    pub(crate) id: usize,
    pub(crate) state: button::State,
    pub(crate) text: String,
}

pub trait GetText {
    fn get_text(&self) -> String;
}

impl GetText for ButtonInfo {
    fn get_text(&self) -> String {
        self.text.clone()
    }
}

pub fn sort_jaro_winkler_button_info<T: GetText>(pattern: &str, candidates: &mut Vec<T>) {
    candidates.sort_unstable_by(|a, b| {
        jaro_winkler(pattern, &b.get_text())
            .partial_cmp(&jaro_winkler(pattern, &a.get_text()))
            .unwrap()
    });
}

pub fn sort_jaro_winkler_button_info2<T: GetText>(
    pattern: &str,
    candidates: &mut IndexMap<usize, T>,
) {
    candidates.sort_by(|_, a, _, b| {
        jaro_winkler(pattern, &b.get_text())
            .partial_cmp(&jaro_winkler(pattern, &a.get_text()))
            .unwrap()
    });
}

impl Default for SectorData {
    fn default() -> Self {
        SectorData {
            start_chrono: Instant::now(),
            ..Default::default()
        }
    }
}

pub struct SectorData {
    pub sector: Sector,
    pub start_mileage: usize,
    pub start_chrono: Instant,
    pub street_data: IndexMap<String, StreetData>,
}

#[derive(Default)]
pub struct StreetData {
    pub street_status: StreetStatus,
    pub street_name: String,
    pub address_data: IndexMap<String, AddressData>,
}

#[derive(Default)]
pub struct AddressData {
    pub address_name: String,
    pub housing_type: HousingType,
    pub heap_size: HeapSize,
    pub heap_data: IndexMap<String, ItemData>,
}

#[derive(Default)]
pub struct ItemData {
    pub item_name: String,
    pub quantity: usize,
    pub condition: ItemCondition,
}

struct OrderedDictionary<V: Serialize> {
    map: IndexMap<String, V>,
}

impl<T: Serialize + Display> OrderedDictionary<T> {
    fn insert(&mut self, key: &str, item: T) -> Option<T> {
        self.map.insert(key.to_string(), item)
    }

    fn remove(&mut self, key: &str) -> Option<T> {
        self.map.remove(key)
    }

    fn contains_key(&self, key: &str) -> bool {
        self.map.contains_key(key)
    }

    fn get(&self, key: &str) -> Option<&T> {
        self.map.get(key)
    }

    fn get_mut(&mut self, key: &str) -> Option<&mut T> {
        self.map.get_mut(key)
    }

    fn get_at_index(&self, index: usize) -> Option<(&String, &T)> {
        self.map.get_index(index)
    }

    fn get_at_index_mut(&mut self, index: usize) -> Option<(&mut String, &mut T)> {
        self.map.get_index_mut(index)
    }

    fn iter_values(&self) -> Values<String, T> {
        self.map.values()
    }

    fn iter_values_mut(&mut self) -> ValuesMut<String, T> {
        self.map.values_mut()
    }

    fn sort_match(&mut self, pattern: &str) {
        self.map.sort_by(|_, a: &T, _, b: &T| {
            jaro_winkler(pattern, &b.to_string())
                .partial_cmp(&jaro_winkler(pattern, &a.to_string()))
                .unwrap()
        });
    }
}

impl<V> Serialize for OrderedDictionary<V>
where
    V: Serialize,
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut map = serializer.serialize_seq(Some(self.map.len()))?;
        for item in self.map.values() {
            map.serialize_element(item)?;
        }
        map.end()
    }
}
