use iced::{
    button, pick_list, text_input, Button, Column, Container, Element, HorizontalAlignment, Length,
    PickList, Row, Text, TextInput, VerticalAlignment,
};

#[derive(Default)]
pub struct EndScreen {
    input_state: text_input::State,
    input_value: String,
    end_button: button::State,
}

#[derive(Debug, Clone)]
pub enum EndScreenMessage {
    InputValueChanged(String),
    EndPressed,
}

impl EndScreen {
    pub fn update(&mut self, message: EndScreenMessage) {
        match message {
            EndScreenMessage::InputValueChanged(value) => {
                if value.is_empty() {
                    self.input_value = String::from("");
                } else if let Ok(nb) = value.parse::<usize>() {
                    self.input_value = value;
                }
            }
            EndScreenMessage::EndPressed => {}
        }
    }

    pub fn view(&mut self) -> Element<EndScreenMessage> {
        let mileage = TextInput::<EndScreenMessage>::new(
            &mut self.input_state,
            "Kilométrage...",
            &self.input_value,
            EndScreenMessage::InputValueChanged,
        );

        let mut end_button = Button::<EndScreenMessage>::new(
            &mut self.end_button,
            Text::new("Terminer")
                .horizontal_alignment(HorizontalAlignment::Center)
                .vertical_alignment(VerticalAlignment::Center)
                .width(Length::Fill),
        );

        if !self.input_value.is_empty() {
            end_button = end_button.on_press(EndScreenMessage::EndPressed);
        }

        Container::new(Column::new().push(mileage).push(end_button)).into()
    }
}
