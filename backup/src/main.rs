use iced::{
    button, Align, Button, Column, Container, Element, HorizontalAlignment, Length, Row, Sandbox,
    Settings, Text, VerticalAlignment,
};

use crate::address_searchbar::{AddressSearchbar, AddressSearchbarMessage};
use crate::end_screen::{EndScreen, EndScreenMessage};
use crate::item_catalog_header::{HeapInfo, ItemCatalogHeader, ItemCatalogHeaderMessage};
use crate::item_sidebar::{ItemSidebar, ItemSidebarMessage};
use crate::selectable_item_catalog::{SelectableItemCatalog, SelectableItemCatalogMessage};
use crate::start_screen::{StartScreen, StartScreenMessage};
use crate::street_searchbar::{StreetSearchbar, StreetSearchbarMessage};
use crate::utils::{GetText, SectorData};
use indexmap::map::IndexMap;
use std::fs;

mod address_searchbar;
mod end_screen;
mod item_catalog_header;
mod item_sidebar;
mod selectable_item_catalog;
mod start_screen;
mod street_searchbar;
mod style;
mod utils;

pub enum AppState {
    Start,
    Main,
    End,
}

impl Default for AppState {
    fn default() -> Self {
        AppState::Start
    }
}

#[derive(Default)]
struct App {
    app_state: AppState,
    street_searchbar: StreetSearchbar,
    address_searchbars: IndexMap<usize, AddressSearchbar>,
    item_catalog_header: ItemCatalogHeader,
    item_sidebar: ItemSidebar,
    selectable_item_catalog: SelectableItemCatalog,
    continue_button: button::State,
    start_screen: StartScreen,
    end_screen: EndScreen,
}

#[derive(Debug, Clone)]
enum AppMessage {
    AddressSearchbarMessage(AddressSearchbarMessage),
    StreetSearchbarMessage(StreetSearchbarMessage),
    ItemCatalogHeaderMessage(ItemCatalogHeaderMessage),
    ItemSidebarMessage(ItemSidebarMessage),
    SelectableItemCatalogMessage(SelectableItemCatalogMessage),
    StartScreenMessage(StartScreenMessage),
    EndScreenMessage(EndScreenMessage),
    MainContinuePressedMessage,
}

impl Sandbox for App {
    type Message = AppMessage;

    fn new() -> Self {
        let file = fs::File::open("data/items.json").expect("file should open read only");
        let items_list: Vec<String> =
            serde_json::from_reader(file).expect("file should be proper JSON");

        let mut app: App = Default::default();

        for item in items_list {
            app.item_sidebar.add_item(item);
        }

        app
    }

    fn title(&self) -> String {
        String::from("Encombrants")
    }

    fn update(&mut self, message: AppMessage) {
        match message {
            AppMessage::AddressSearchbarMessage(message) => {
                let searchbar = self
                    .address_searchbars
                    .get_mut(&self.street_searchbar.selected_button.unwrap())
                    .unwrap();

                searchbar.update(message.clone());

                if let AddressSearchbarMessage::ItemSubmit = message {
                    self.item_catalog_header.heaps.insert(
                        (
                            self.street_searchbar.selected_button.unwrap(),
                            searchbar.selected_button.unwrap(),
                        ),
                        HeapInfo::default(),
                    );

                    self.selectable_item_catalog.item_list.insert(
                        (
                            self.street_searchbar.selected_button.unwrap(),
                            searchbar.selected_button.unwrap(),
                        ),
                        IndexMap::new(),
                    );
                }
            }
            AppMessage::StreetSearchbarMessage(message) => {
                self.street_searchbar.update(message.clone());
                if let StreetSearchbarMessage::ItemSubmit = message {
                    if !self
                        .street_searchbar
                        .contains(&self.street_searchbar.input_value)
                    {
                        self.address_searchbars.insert(
                            self.street_searchbar.selected_button.expect(
                                "there should be a selected button after adding new street",
                            ),
                            AddressSearchbar::default(),
                        );
                    }
                }
            }
            AppMessage::ItemCatalogHeaderMessage(message) => {
                let street_id = self.street_searchbar.selected_button.unwrap();
                let adress_id = self
                    .address_searchbars
                    .get(&self.street_searchbar.selected_button.unwrap())
                    .unwrap()
                    .selected_button
                    .unwrap();

                self.item_catalog_header
                    .update(message, street_id, adress_id);
            }
            AppMessage::ItemSidebarMessage(message) => {
                if let ItemSidebarMessage::ConfirmButtonPressed = &message {
                    if let Some(street_id) = self.street_searchbar.selected_button {
                        if let Some(adress_id) = self
                            .address_searchbars
                            .get(&street_id)
                            .unwrap()
                            .selected_button
                        {
                            if let Some(item_id) = self.item_sidebar.selected_button {
                                self.selectable_item_catalog.add_item(
                                    street_id,
                                    adress_id,
                                    self.item_sidebar.selected_button.unwrap(),
                                    self.item_sidebar.nb_input_value.parse().unwrap(),
                                    self.item_sidebar.buttons.get(&item_id).unwrap().get_text(),
                                    self.item_sidebar.selected_item_condition,
                                );
                                self.item_sidebar.reset();
                            }
                        }
                    };
                }
                self.item_sidebar.update(message);
            }
            AppMessage::SelectableItemCatalogMessage(message) => {
                let x = message.clone();
                match message {
                    SelectableItemCatalogMessage::AddButtonPressed => {
                        self.item_sidebar.reset();
                    }
                    SelectableItemCatalogMessage::RemoveButtonPressed => {
                        let street_id = self.street_searchbar.selected_button.unwrap();
                        let adress_id = self
                            .address_searchbars
                            .get(&self.street_searchbar.selected_button.unwrap())
                            .unwrap()
                            .selected_button
                            .unwrap();

                        self.selectable_item_catalog
                            .remove_item(street_id, adress_id);
                    }
                    SelectableItemCatalogMessage::ItemPressed(
                        row_id,
                        item_id,
                        quantity,
                        condition,
                    ) => {
                        let street_id = self.street_searchbar.selected_button.unwrap();
                        let adress_id = self
                            .address_searchbars
                            .get(&self.street_searchbar.selected_button.unwrap())
                            .unwrap()
                            .selected_button
                            .unwrap();
                        self.item_sidebar.selected_button = Some(item_id);
                        self.item_sidebar.selected_item_condition = condition;
                        self.item_sidebar.nb_input_value = quantity.clone();
                    }
                }
                self.selectable_item_catalog.update(x);
            }
            AppMessage::StartScreenMessage(message) => {
                if let StartScreenMessage::ContinuePressed = message {
                    let file_path = format!(
                        "data/{}.json",
                        self.start_screen.selected_sector.to_string()
                    );
                    let file = fs::File::open(file_path).expect("file should open read only");
                    let streets_list: Vec<String> =
                        serde_json::from_reader(file).expect("file should be proper JSON");

                    for street in streets_list {
                        self.street_searchbar.add_item(street);

                        self.address_searchbars.insert(
                            self.street_searchbar.selected_button.expect(
                                "there should be a selected button after adding new street",
                            ),
                            AddressSearchbar::default(),
                        );
                    }
                    self.app_state = AppState::Main;
                }
                self.start_screen.update(message);
            }
            AppMessage::EndScreenMessage(message) => {
                self.end_screen.update(message);
            }
            AppMessage::MainContinuePressedMessage => {
                self.app_state = AppState::End;
            }
        }
    }

    fn view(&mut self) -> Element<AppMessage> {
        match &self.app_state {
            AppState::Start => self.start_screen.view().map(AppMessage::StartScreenMessage),
            AppState::Main => {
                let address_string = match &mut self.street_searchbar.selected_button {
                    None => String::from(""),
                    Some(streetindex) => {
                        let street_name = self
                            .street_searchbar
                            .buttons
                            .get(streetindex)
                            .unwrap()
                            .get_text();

                        let street_number = match &self
                            .address_searchbars
                            .get(streetindex)
                            .unwrap()
                            .selected_button
                        {
                            None => String::from(""),
                            Some(number_index) => self
                                .address_searchbars
                                .get(streetindex)
                                .unwrap()
                                .buttons
                                .get(number_index)
                                .unwrap()
                                .get_text(),
                        };

                        format!("{} {}", street_number, street_name)
                    }
                };

                let (
                    selectable_item_catalog_container,
                    item_sidebar_container,
                    item_catalog_header_container,
                ) = match self.street_searchbar.selected_button {
                    Some(street_id) => {
                        match self
                            .address_searchbars
                            .get(&street_id)
                            .unwrap()
                            .selected_button
                        {
                            Some(adress_id) => (
                                Container::<AppMessage>::new(
                                    self.selectable_item_catalog
                                        .view(street_id, adress_id)
                                        .map(AppMessage::SelectableItemCatalogMessage),
                                ),
                                Container::<AppMessage>::new(
                                    self.item_sidebar.view().map(AppMessage::ItemSidebarMessage),
                                ),
                                Container::<AppMessage>::new(
                                    self.item_catalog_header
                                        .view(street_id, adress_id)
                                        .map(AppMessage::ItemCatalogHeaderMessage),
                                ),
                            ),
                            None => (
                                Container::new(
                                    Text::new("vide")
                                        .horizontal_alignment(HorizontalAlignment::Center)
                                        .vertical_alignment(VerticalAlignment::Center)
                                        .height(Length::Fill)
                                        .width(Length::Fill),
                                ),
                                Container::new(
                                    Text::new("vide")
                                        .horizontal_alignment(HorizontalAlignment::Center)
                                        .vertical_alignment(VerticalAlignment::Center)
                                        .height(Length::Fill)
                                        .width(Length::Fill),
                                ),
                                Container::new(
                                    Text::new("vide")
                                        .horizontal_alignment(HorizontalAlignment::Center)
                                        .vertical_alignment(VerticalAlignment::Center)
                                        .height(Length::Fill)
                                        .width(Length::Fill),
                                ),
                            ),
                        }
                    }
                    None => (
                        Container::new(
                            Text::new("vide")
                                .horizontal_alignment(HorizontalAlignment::Center)
                                .vertical_alignment(VerticalAlignment::Center)
                                .height(Length::Fill)
                                .width(Length::Fill),
                        ),
                        Container::new(
                            Text::new("vide")
                                .horizontal_alignment(HorizontalAlignment::Center)
                                .vertical_alignment(VerticalAlignment::Center)
                                .height(Length::Fill)
                                .width(Length::Fill),
                        ),
                        Container::new(
                            Text::new("vide")
                                .horizontal_alignment(HorizontalAlignment::Center)
                                .vertical_alignment(VerticalAlignment::Center)
                                .height(Length::Fill)
                                .width(Length::Fill),
                        ),
                    ),
                };

                let address_searchbar_container = match &self.street_searchbar.selected_button {
                    Some(id) => Container::<AppMessage>::new(
                        self.address_searchbars
                            .get_mut(id)
                            .unwrap()
                            .view()
                            .map(AppMessage::AddressSearchbarMessage),
                    ),
                    None => Container::new(
                        Text::new("vide")
                            .horizontal_alignment(HorizontalAlignment::Center)
                            .vertical_alignment(VerticalAlignment::Center)
                            .height(Length::Fill)
                            .width(Length::Fill),
                    ),
                };

                let street_searchbar_container = Container::<AppMessage>::new(
                    self.street_searchbar
                        .view()
                        .map(AppMessage::StreetSearchbarMessage),
                );

                let continue_button = Button::<AppMessage>::new(
                    &mut self.continue_button,
                    Text::new("Continuer")
                        .horizontal_alignment(HorizontalAlignment::Center)
                        .vertical_alignment(VerticalAlignment::Center),
                )
                .on_press(AppMessage::MainContinuePressedMessage);

                Container::new(
                    Container::new(
                        Row::new()
                            .push(
                                // Street searchbar
                                Container::new(street_searchbar_container)
                                    .width(Length::Units(300))
                                    .height(Length::Fill)
                                    .center_x()
                                    .center_y()
                                    .style(style::Container),
                            )
                            .push(
                                // Address searchbar
                                Container::new(address_searchbar_container)
                                    .width(Length::Units(200))
                                    .height(Length::Fill)
                                    .center_x()
                                    .center_y()
                                    .style(style::Container),
                            )
                            .push(
                                Container::new(
                                    Column::new()
                                        .push(
                                            Row::new()
                                                .push(
                                                    Text::new(address_string)
                                                        .size(30)
                                                        .width(Length::Units(200))
                                                        .height(Length::Fill)
                                                        .horizontal_alignment(
                                                            HorizontalAlignment::Center,
                                                        )
                                                        .vertical_alignment(
                                                            VerticalAlignment::Center,
                                                        ),
                                                )
                                                .push(
                                                    Row::new()
                                                        .push(
                                                            item_catalog_header_container
                                                                .padding(10)
                                                                .width(Length::Fill)
                                                                .height(Length::Fill)
                                                                .align_x(Align::Center)
                                                                .align_y(Align::Center),
                                                        )
                                                        .push(continue_button),
                                                )
                                                .width(Length::Fill)
                                                .height(Length::Units(60)),
                                        )
                                        .push(
                                            Container::new(
                                                Row::new()
                                                    .push(
                                                        item_sidebar_container
                                                            .height(Length::Fill)
                                                            .width(Length::Units(300))
                                                            .style(style::Container),
                                                    )
                                                    .push(
                                                        selectable_item_catalog_container
                                                            .height(Length::Fill)
                                                            .width(Length::Fill)
                                                            .style(style::Container),
                                                    ),
                                            )
                                            .width(Length::Fill)
                                            .height(Length::Fill)
                                            .style(style::Container),
                                        ),
                                )
                                .width(Length::Fill)
                                .height(Length::Fill)
                                .center_x()
                                .center_y()
                                .style(style::Container),
                            )
                            .spacing(30),
                    )
                    .height(Length::Fill)
                    .center_y()
                    .style(style::Container),
                )
                .padding(30)
                .height(Length::Fill)
                .width(Length::Fill)
                .center_y()
                .style(style::Container)
                .into()
            }
            AppState::End => self.end_screen.view().map(AppMessage::EndScreenMessage),
        }
    }
}

fn main() -> iced::Result {
    App::run(Settings::default())
}
