use iced::{
    button, scrollable, text_input, Align, Button, Column, Container, Element, HorizontalAlignment,
    Length, Row, Sandbox, Scrollable, Svg, Text, TextInput, VerticalAlignment,
};
use indexmap::*;

use crate::street_searchbar::StreetSearchbarMessage::ItemStatusChanged;
use crate::utils::{sort_jaro_winkler_button_info2, ButtonInfo};
use crate::{style, utils};

pub enum StreetStatus {
    Empty,
    InProgress,
    Complete,
}

impl Default for StreetStatus {
    fn default() -> Self {
        StreetStatus::Empty
    }
}

pub struct SelectableStatusButton {
    pub status: StreetStatus,
    pub status_button_state: button::State,
    pub selectable_button: ButtonInfo,
}

impl SelectableStatusButton {
    fn next_status(&mut self) {
        self.status = match self.status {
            StreetStatus::Empty => StreetStatus::InProgress,
            StreetStatus::InProgress => StreetStatus::Complete,
            StreetStatus::Complete => StreetStatus::InProgress,
        };
    }
}

impl utils::GetText for SelectableStatusButton {
    fn get_text(&self) -> String {
        self.selectable_button.text.clone()
    }
}

impl Default for Icons {
    fn default() -> Self {
        Icons {
            check: Svg::from_path(format!(
                "{}/resources/check.svg",
                env!("CARGO_MANIFEST_DIR")
            ))
            .height(Length::Units(20))
            .width(Length::Units(20)),
            empty: Svg::from_path(format!(
                "{}/resources/empty.svg",
                env!("CARGO_MANIFEST_DIR")
            ))
            .height(Length::Units(20))
            .width(Length::Units(20)),
            half: Svg::from_path(format!("{}/resources/half.svg", env!("CARGO_MANIFEST_DIR")))
                .height(Length::Units(20))
                .width(Length::Units(20)),
        }
    }
}

struct Icons {
    check: Svg,
    empty: Svg,
    half: Svg,
}

#[derive(Default)]
pub(crate) struct StreetSearchbar {
    pub(crate) buttons: IndexMap<usize, SelectableStatusButton>,
    icons: Icons,
    pub(crate) available_id: usize,
    input_state: text_input::State,
    pub(crate) input_value: String,
    remove_button_state: button::State,
    pub selected_button: Option<usize>,
    scrollable_state: scrollable::State,
}

impl StreetSearchbar {
    pub(crate) fn add_item(&mut self, item: String) {
        if !self.contains(&item) {
            self.buttons.insert(
                self.available_id,
                SelectableStatusButton {
                    status: StreetStatus::Empty,
                    status_button_state: Default::default(),
                    selectable_button: ButtonInfo {
                        id: self.available_id,
                        state: Default::default(),
                        text: item,
                    },
                },
            );
            self.selected_button = Some(self.available_id);
            self.available_id += 1;
        }
    }

    pub(crate) fn contains(&self, pattern: &str) -> bool {
        self.buttons
            .iter()
            .map(|x| &x.1.selectable_button.text)
            .any(|x| x == pattern)
    }
}

#[derive(Debug, Clone)]
pub enum StreetSearchbarMessage {
    InputChanged(String),
    ItemPressed(usize),
    ItemSubmit,
    ItemRemove,
    ItemStatusChanged(usize),
}

impl Sandbox for StreetSearchbar {
    type Message = StreetSearchbarMessage;

    fn new() -> Self {
        Default::default()
    }

    fn title(&self) -> String {
        String::from("Searchbar")
    }

    fn update(&mut self, message: Self::Message) {
        match message {
            StreetSearchbarMessage::InputChanged(value) => {
                self.input_value = value;
                sort_jaro_winkler_button_info2(&self.input_value, &mut self.buttons);
            }
            StreetSearchbarMessage::ItemPressed(id) => self.selected_button = Some(id),
            StreetSearchbarMessage::ItemSubmit => {
                if !self.contains(&self.input_value) {
                    self.buttons.insert(
                        self.available_id,
                        SelectableStatusButton {
                            status: StreetStatus::Empty,
                            status_button_state: Default::default(),
                            selectable_button: ButtonInfo {
                                id: self.available_id,
                                state: Default::default(),
                                text: self.input_value.clone(),
                            },
                        },
                    );

                    self.selected_button = Some(self.available_id);
                    self.available_id += 1;
                    self.input_value = String::from("");
                }
            }
            StreetSearchbarMessage::ItemRemove => {
                if let Some(selected) = self.selected_button {
                    self.buttons.remove(&selected);
                    self.selected_button = None;
                }
            }
            StreetSearchbarMessage::ItemStatusChanged(id) => {
                self.buttons
                    .get_mut(&id)
                    .expect("expected id in buttons map")
                    .next_status();
            }
        }
    }

    fn view(&mut self) -> Element<StreetSearchbarMessage> {
        let mut scrollable_list =
            Column::<StreetSearchbarMessage>::new().width(Length::FillPortion(95));

        // List of selectable buttons
        for button in &mut self.buttons.values_mut() {
            let is_selected = match self.selected_button {
                Some(selected) => selected == button.selectable_button.id,
                None => false,
            };

            let svg = match button.status {
                StreetStatus::Empty => self.icons.empty.clone(),
                StreetStatus::InProgress => self.icons.half.clone(),
                StreetStatus::Complete => self.icons.check.clone(),
            };

            scrollable_list = scrollable_list.push(
                Row::new()
                    .push(
                        Button::new(&mut button.status_button_state, svg)
                            .on_press(ItemStatusChanged(button.selectable_button.id)),
                    )
                    .push(
                        Button::new(
                            &mut button.selectable_button.state,
                            Text::new(button.selectable_button.text.clone())
                                .horizontal_alignment(HorizontalAlignment::Center)
                                .vertical_alignment(VerticalAlignment::Center),
                        )
                        .width(Length::Fill)
                        .padding(10)
                        .on_press(StreetSearchbarMessage::ItemPressed(
                            button.selectable_button.id,
                        ))
                        .style(style::Button { is_selected }),
                    )
                    .spacing(10)
                    .align_items(Align::Center),
            )
        }

        // Content which wraps the textbox, scrollable list and remove button widgets
        Container::new(
            Column::new()
                .push(
                    // Text input
                    TextInput::<StreetSearchbarMessage>::new(
                        &mut self.input_state,
                        "Rue...",
                        &self.input_value,
                        StreetSearchbarMessage::InputChanged,
                    )
                    .padding(10)
                    .width(Length::Fill)
                    .on_submit(StreetSearchbarMessage::ItemSubmit),
                )
                .push(
                    // Scrollable list
                    Scrollable::<StreetSearchbarMessage>::new(&mut self.scrollable_state)
                        .push(
                            Row::<StreetSearchbarMessage>::new()
                                .push(scrollable_list)
                                .push(
                                    Column::<StreetSearchbarMessage>::new()
                                        .width(Length::FillPortion(5)),
                                ),
                        )
                        .align_items(Align::Center)
                        .height(Length::Fill)
                        .width(Length::Fill),
                )
                .push(
                    // Delete button
                    Button::new(
                        &mut self.remove_button_state,
                        Text::new("Retirer")
                            .horizontal_alignment(HorizontalAlignment::Center)
                            .vertical_alignment(VerticalAlignment::Center)
                            .width(Length::Fill),
                    )
                    .width(Length::Fill)
                    .height(Length::Units(30))
                    .on_press(StreetSearchbarMessage::ItemRemove)
                    .style(style::RemoveButton),
                )
                .align_items(Align::Center)
                .padding(20)
                .spacing(10),
        )
        .style(style::Container)
        .into()
    }
}
