use iced::{
    button, pick_list, text_input, Button, Column, Container, Element, HorizontalAlignment, Length,
    PickList, Row, Text, TextInput, VerticalAlignment,
};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Sector {
    Sector1,
    Sector2,
    Sector3,
    Sector4,
    Sector5,
    Sector6,
    Sector7,
    Sector8,
    Sector9,
    Sector10,
    Sector11,
}

impl Sector {
    const ALL: [Sector; 11] = [
        Sector::Sector1,
        Sector::Sector2,
        Sector::Sector3,
        Sector::Sector4,
        Sector::Sector5,
        Sector::Sector6,
        Sector::Sector7,
        Sector::Sector8,
        Sector::Sector9,
        Sector::Sector10,
        Sector::Sector11,
    ];
}

impl Default for Sector {
    fn default() -> Self {
        Sector::Sector1
    }
}

impl std::fmt::Display for Sector {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Secteur {}",
            match self {
                Sector::Sector1 => "1",
                Sector::Sector2 => "2",
                Sector::Sector3 => "3",
                Sector::Sector4 => "4",
                Sector::Sector5 => "5",
                Sector::Sector6 => "6",
                Sector::Sector7 => "7",
                Sector::Sector8 => "8",
                Sector::Sector9 => "9",
                Sector::Sector10 => "10",
                Sector::Sector11 => "11",
            }
        )
    }
}

#[derive(Default)]
pub struct StartScreen {
    sector_picklist: pick_list::State<Sector>,
    pub(crate) selected_sector: Sector,
    mileage_input_state: text_input::State,
    pub(crate) mileage_input_value: String,
    continue_button: button::State,
}

#[derive(Debug, Clone)]
pub enum StartScreenMessage {
    SectorSelected(Sector),
    InputValueChanged(String),
    ContinuePressed,
}

impl StartScreen {
    pub fn update(&mut self, message: StartScreenMessage) {
        match message {
            StartScreenMessage::SectorSelected(sector) => {
                self.selected_sector = sector;
            }
            StartScreenMessage::InputValueChanged(value) => {
                if value.is_empty() {
                    self.mileage_input_value = String::from("");
                } else if let Ok(nb) = value.parse::<usize>() {
                    self.mileage_input_value = value;
                }
            }
            StartScreenMessage::ContinuePressed => {}
        }
    }

    pub fn view(&mut self) -> Element<StartScreenMessage> {
        let sector_pick_list = PickList::new(
            &mut self.sector_picklist,
            &Sector::ALL[..],
            Some(self.selected_sector),
            StartScreenMessage::SectorSelected,
        );

        let mileage = TextInput::<StartScreenMessage>::new(
            &mut self.mileage_input_state,
            "Kilométrage...",
            &self.mileage_input_value,
            StartScreenMessage::InputValueChanged,
        );

        let mut continue_button = Button::<StartScreenMessage>::new(
            &mut self.continue_button,
            Text::new("Continuer")
                .horizontal_alignment(HorizontalAlignment::Center)
                .vertical_alignment(VerticalAlignment::Center),
        );

        if !self.mileage_input_value.is_empty() {
            continue_button = continue_button.on_press(StartScreenMessage::ContinuePressed);
        }

        Container::new(
            Column::new()
                .push(sector_pick_list)
                .push(mileage)
                .push(continue_button),
        )
        .into()
    }
}
