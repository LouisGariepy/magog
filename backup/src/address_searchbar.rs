use iced::{
    button, scrollable, text_input, Align, Button, Column, Container, Element, HorizontalAlignment,
    Length, Row, Sandbox, Scrollable, Text, TextInput, VerticalAlignment,
};

use crate::utils::{sort_jaro_winkler_button_info, sort_jaro_winkler_button_info2, ButtonInfo};
use crate::{style, utils};
use indexmap::map::IndexMap;

#[derive(Default)]
pub(crate) struct AddressSearchbar {
    pub buttons: IndexMap<usize, ButtonInfo>,
    available_id: usize,
    input_state: text_input::State,
    input_value: String,
    remove_button_state: button::State,
    pub selected_button: Option<usize>,
    scrollable_state: scrollable::State,
}

#[derive(Debug, Clone)]
pub enum AddressSearchbarMessage {
    InputChanged(String),
    ItemPressed(usize),
    ItemSubmit,
    ItemRemove,
}

impl Sandbox for AddressSearchbar {
    type Message = AddressSearchbarMessage;

    fn new() -> Self {
        Self::default()
    }

    fn title(&self) -> String {
        String::from("Searchbar")
    }

    fn update(&mut self, message: Self::Message) {
        match message {
            AddressSearchbarMessage::InputChanged(value) => {
                self.input_value = value;
                sort_jaro_winkler_button_info2(&self.input_value, &mut self.buttons);
            }
            AddressSearchbarMessage::ItemPressed(id) => self.selected_button = Some(id),
            AddressSearchbarMessage::ItemSubmit => {
                if !self
                    .buttons
                    .iter()
                    .map(|x| &x.1.text)
                    .any(|x| x == &self.input_value)
                {
                    self.buttons.insert(
                        self.available_id,
                        ButtonInfo {
                            id: self.available_id,
                            state: Default::default(),
                            text: self.input_value.clone(),
                        },
                    );

                    self.selected_button = Some(self.available_id);
                    self.available_id += 1;
                    self.input_value = String::from("");
                }
            }
            AddressSearchbarMessage::ItemRemove => {
                if let Some(selected) = self.selected_button {
                    self.buttons.remove(&selected);
                    self.selected_button = None;
                }
            }
        }
    }

    fn view(&mut self) -> Element<AddressSearchbarMessage> {
        let mut scrollable_list =
            Column::<AddressSearchbarMessage>::new().width(Length::FillPortion(95));

        // List of selectable buttons
        for button in &mut self.buttons.values_mut() {
            let is_selected = match self.selected_button {
                Some(selected) => selected == button.id,
                None => false,
            };

            scrollable_list = scrollable_list.push(
                Button::new(
                    &mut button.state,
                    Text::new(button.text.clone())
                        .horizontal_alignment(HorizontalAlignment::Center)
                        .width(Length::Fill),
                )
                .style(style::Button { is_selected })
                .width(Length::Fill)
                .on_press(AddressSearchbarMessage::ItemPressed(button.id)),
            );
        }

        // Content which wraps the textbox, scrollable list and remove button widgets
        Container::new(
            Column::new()
                .push(
                    // Text input
                    TextInput::<AddressSearchbarMessage>::new(
                        &mut self.input_state,
                        "Adresse...",
                        &self.input_value,
                        AddressSearchbarMessage::InputChanged,
                    )
                    .padding(10)
                    .width(Length::Fill)
                    .on_submit(AddressSearchbarMessage::ItemSubmit),
                )
                .push(
                    // Scrollable list
                    Scrollable::<AddressSearchbarMessage>::new(&mut self.scrollable_state)
                        .push(
                            Row::<AddressSearchbarMessage>::new()
                                .push(scrollable_list)
                                .push(
                                    Column::<AddressSearchbarMessage>::new()
                                        .width(Length::FillPortion(5)),
                                ),
                        )
                        .align_items(Align::Center)
                        .height(Length::Fill)
                        .width(Length::Fill),
                )
                .push(
                    // Delete button
                    Button::new(
                        &mut self.remove_button_state,
                        Text::new("Retirer")
                            .horizontal_alignment(HorizontalAlignment::Center)
                            .vertical_alignment(VerticalAlignment::Center)
                            .width(Length::Fill),
                    )
                    .width(Length::Fill)
                    .height(Length::Units(30))
                    .on_press(AddressSearchbarMessage::ItemRemove)
                    .style(style::RemoveButton),
                )
                .align_items(Align::Center)
                .padding(20)
                .spacing(10),
        )
        .style(style::Container)
        .into()
    }
}
