use iced::{
    button, Align, Button, Column, Container, Element, HorizontalAlignment, Length, Row, Sandbox,
    Settings, Text, VerticalAlignment,
};

use crate::address_searchbar::{AddressSearchbar, AddressSearchbarMessage};
use crate::end_screen::{EndScreen, EndScreenMessage};
use crate::item_catalog_header::{HeapInfo, ItemCatalogHeader, ItemCatalogHeaderMessage};
use crate::item_sidebar::{ItemSidebar, ItemSidebarMessage};
use crate::selectable_item_catalog::{
    ItemRowButton, SelectableItemCatalog, SelectableItemCatalogMessage,
};
use crate::start_screen::{StartScreen, StartScreenMessage};
use crate::street_searchbar::{StreetSearchbar, StreetSearchbarMessage};
use crate::utils::{AddressData, Dictionary, ItemData, SectorData, StreetData};
use chrono::Local;
use std::fs;
use std::process::exit;
use std::time::Instant;

mod address_searchbar;
mod end_screen;
mod item_catalog_header;
mod item_sidebar;
mod selectable_item_catalog;
mod start_screen;
mod street_searchbar;
mod style;
mod utils;

pub enum AppState {
    Start,
    Main,
    End,
}

impl Default for AppState {
    fn default() -> Self {
        AppState::Start
    }
}

struct App {
    start_chrono: Instant,
    app_state: AppState,
    street_searchbar: StreetSearchbar,
    address_searchbars: Dictionary<AddressSearchbar>,
    item_catalog_header: ItemCatalogHeader,
    item_sidebar: ItemSidebar,
    selectable_item_catalog: SelectableItemCatalog,
    continue_button: button::State,
    start_screen: StartScreen,
    end_screen: EndScreen,
}

impl Default for App {
    fn default() -> Self {
        App {
            start_chrono: Instant::now(),
            app_state: Default::default(),
            street_searchbar: Default::default(),
            address_searchbars: Default::default(),
            item_catalog_header: Default::default(),
            item_sidebar: Default::default(),
            selectable_item_catalog: Default::default(),
            continue_button: Default::default(),
            start_screen: Default::default(),
            end_screen: Default::default(),
        }
    }
}

impl App {
    fn selected_buttons(&self) -> (Option<String>, Option<String>, Option<String>) {
        match &self.street_searchbar.selected_button {
            Some(street) => match &self.address_searchbars.get(street).unwrap().selected_button {
                Some(address) => match &self.item_sidebar.selected_button {
                    Some(item) => (
                        Some(street.clone()),
                        Some(address.clone()),
                        Some(item.clone()),
                    ),
                    None => (Some(street.clone()), Some(address.clone()), None),
                },
                None => (Some(street.clone()), None, None),
            },
            None => (None, None, None),
        }
    }

    fn get_address_searchbar(&mut self) -> Option<&AddressSearchbar> {
        match &self.selected_buttons() {
            (Some(street), _, _) => self.address_searchbars.get(&street),
            _ => None,
        }
    }

    fn get_address_searchbar_mut(&mut self) -> Option<&mut AddressSearchbar> {
        match &self.selected_buttons() {
            (Some(street), _, _) => self.address_searchbars.get_mut(&street),
            _ => None,
        }
    }

    fn get_catalog_header(&mut self) -> Option<&HeapInfo> {
        match &self.selected_buttons() {
            (Some(street), Some(address), _) => self
                .item_catalog_header
                .heaps
                .get(&format!("{}{}", street, address)),
            _ => None,
        }
    }

    fn get_catalog(&mut self) -> Option<&Vec<ItemRowButton>> {
        match &self.selected_buttons() {
            (Some(street), Some(address), _) => self
                .selectable_item_catalog
                .item_list
                .get(&format!("{}{}", street, address)),
            _ => None,
        }
    }
}

impl App {
    fn to_data(&mut self) -> SectorData {
        let start_mileage = self
            .start_screen
            .mileage_input_value
            .parse::<usize>()
            .unwrap();

        let end_mileage = self
            .end_screen
            .mileage_input_value
            .parse::<usize>()
            .unwrap_or(0);

        let duration = {
            let duration = self.start_chrono.elapsed();
            let seconds = duration.as_secs() % 60;
            let minutes = (duration.as_secs() / 60) % 60;
            let hours = (duration.as_secs() / 60) / 60;
            format!("{}:{}:{}", hours, minutes, seconds)
        };

        let date = Local::now().to_string();

        let mut sector_data = SectorData {
            sector: self.start_screen.selected_sector,
            mileage: end_mileage as i32 - start_mileage as i32,
            date,
            duration,
            street_data: Vec::new(),
        };

        for street in self.street_searchbar.buttons.iter_values() {
            let street_name = street.selectable_button.text.clone();
            let street_status = street.status.clone();

            let mut street_data = StreetData {
                street_name: street_name.clone(),
                street_status,
                address_data: Vec::new(),
            };

            for address in self
                .address_searchbars
                .get(&street_name)
                .unwrap()
                .buttons
                .iter_values()
            {
                let address_name = address.text.clone();

                let heap_info = self
                    .item_catalog_header
                    .heaps
                    .get(&format!("{}{}", &street_name, &address_name))
                    .unwrap();

                let mut address_data = AddressData {
                    address_name: address_name.clone(),
                    housing_type: heap_info.selected_housing,
                    heap_size: heap_info.selected_heapsize,
                    heap_data: Vec::new(),
                };

                for item in self
                    .selectable_item_catalog
                    .item_list
                    .get(&format!("{}{}", &street_name, &address_name))
                    .unwrap()
                {
                    let item_data = ItemData {
                        item_name: item.item_row.name.clone(),
                        quantity: item.item_row.quantity,
                        condition: item.item_row.condition,
                    };

                    address_data.heap_data.push(item_data);
                }

                street_data.address_data.push(address_data);
            }

            sector_data.street_data.push(street_data)
        }

        sector_data
    }
}

#[derive(Debug, Clone)]
enum AppMessage {
    AddressSearchbarMessage(AddressSearchbarMessage),
    StreetSearchbarMessage(StreetSearchbarMessage),
    ItemCatalogHeaderMessage(ItemCatalogHeaderMessage),
    ItemSidebarMessage(ItemSidebarMessage),
    SelectableItemCatalogMessage(SelectableItemCatalogMessage),
    StartScreenMessage(StartScreenMessage),
    EndScreenMessage(EndScreenMessage),
    MainContinuePressedMessage,
}

impl Sandbox for App {
    type Message = AppMessage;

    fn new() -> Self {
        let file = fs::File::open("data/items.json").expect("file should open read only");
        let items_list: Vec<String> =
            serde_json::from_reader(file).expect("file should be proper JSON");

        let mut app: App = App {
            start_chrono: Instant::now(),
            ..Default::default()
        };

        for item in items_list {
            app.item_sidebar.add_item(item);
        }

        app
    }

    fn title(&self) -> String {
        String::from("Encombrants")
    }

    fn update(&mut self, message: AppMessage) {
        match message {
            AppMessage::AddressSearchbarMessage(message) => {
                if let Some(searchbar) = self.get_address_searchbar_mut() {
                    match message {
                        AddressSearchbarMessage::ItemSubmit => {
                            if !searchbar.buttons.contains_key(&searchbar.input_value) {
                                searchbar.update(message);

                                if let (Some(street), Some(address), _) = self.selected_buttons() {
                                    self.item_catalog_header.heaps.insert(
                                        &format!("{}{}", street, address),
                                        HeapInfo::default(),
                                    );

                                    self.selectable_item_catalog.item_list.insert(
                                        &format!("{}{}", street, address),
                                        Default::default(),
                                    );
                                }

                                self.item_sidebar.reset();
                                self.selectable_item_catalog.selected_button = None;
                            } else {
                                return;
                            }
                        }
                        AddressSearchbarMessage::ItemPressed(_) => {
                            searchbar.update(message);
                            self.item_sidebar.reset();
                            self.selectable_item_catalog.selected_button = None;
                        }
                        _ => {
                            searchbar.update(message);
                        }
                    }
                }
            }
            AppMessage::StreetSearchbarMessage(message) => {
                self.street_searchbar.update(message.clone());
                if let StreetSearchbarMessage::ItemSubmit = message {
                    if !self
                        .street_searchbar
                        .buttons
                        .contains_key(&self.street_searchbar.input_value)
                    {
                        self.address_searchbars.insert(
                            self.street_searchbar.selected_button.as_ref().expect(
                                "there should be a selected button after adding new street",
                            ),
                            AddressSearchbar::default(),
                        );
                    }
                } else if let StreetSearchbarMessage::ItemPressed(_) = message {
                    self.item_sidebar.reset();
                    self.selectable_item_catalog.selected_button = None;
                }
            }
            AppMessage::ItemCatalogHeaderMessage(message) => {
                if let (Some(street), Some(address), _) = self.selected_buttons() {
                    self.item_catalog_header.update(message, &street, &address);
                }
            }

            AppMessage::ItemSidebarMessage(message) => {
                if let ItemSidebarMessage::ConfirmButtonPressed = &message {
                    if let (Some(street), Some(address), Some(item)) = self.selected_buttons() {
                        self.selectable_item_catalog.add_item(
                            street,
                            address,
                            item,
                            self.item_sidebar.nb_input_value.parse().unwrap(),
                            self.item_sidebar.selected_item_condition,
                        );
                        self.item_sidebar.reset();
                        self.selectable_item_catalog.selected_button = None;
                    };
                    let json = serde_json::to_string_pretty(&self.to_data()).unwrap();
                    fs::write("out/data.json", json).expect("Unable to write file");
                }
                self.item_sidebar.update(message);
            }
            AppMessage::SelectableItemCatalogMessage(message) => {
                let x = message.clone();
                match message {
                    SelectableItemCatalogMessage::AddButtonPressed => {
                        self.item_sidebar.reset();
                        self.selectable_item_catalog.selected_button = None;
                    }
                    SelectableItemCatalogMessage::RemoveButtonPressed => {
                        if let (Some(street), Some(address), _) = self.selected_buttons() {
                            self.selectable_item_catalog.remove_item(street, address);
                            self.item_sidebar.reset();
                            self.selectable_item_catalog.selected_button = None;
                        }
                    }
                    SelectableItemCatalogMessage::ItemPressed(_, item, quantity, condition) => {
                        if let (Some(street), Some(address), _) = self.selected_buttons() {
                            self.item_sidebar.selected_button = Some(item);
                            self.item_sidebar.selected_item_condition = condition;
                            self.item_sidebar.nb_input_value = quantity;
                        }
                    }
                }
                self.selectable_item_catalog.update(x);
            }
            AppMessage::StartScreenMessage(message) => {
                if let StartScreenMessage::ContinuePressed = message {
                    self.start_chrono = Instant::now();

                    let file_path = format!(
                        "data/{}.json",
                        self.start_screen.selected_sector.to_string()
                    );
                    let file = fs::File::open(file_path).expect("file should open read only");
                    let streets_list: Vec<String> =
                        serde_json::from_reader(file).expect("file should be proper JSON");

                    for street in streets_list {
                        self.street_searchbar.add_item(street);

                        self.address_searchbars.insert(
                            self.street_searchbar.selected_button.as_ref().expect(
                                "there should be a selected button after adding new street",
                            ),
                            AddressSearchbar::default(),
                        );
                    }
                    self.street_searchbar.selected_button = None;
                    self.app_state = AppState::Main;
                }
                self.start_screen.update(message);
            }
            AppMessage::EndScreenMessage(message) => {
                self.end_screen.update(message.clone());
                if let EndScreenMessage::EndPressed = message {
                    let json = serde_json::to_string_pretty(&self.to_data()).unwrap();
                    fs::write("out/data.json", json).expect("Unable to write file");
                    exit(0);
                }
            }
            AppMessage::MainContinuePressedMessage => {
                self.app_state = AppState::End;
            }
        }
    }

    fn view(&mut self) -> Element<AppMessage> {
        match &self.app_state {
            AppState::Start => self.start_screen.view().map(AppMessage::StartScreenMessage),
            AppState::Main => {
                let address_string = match self.selected_buttons() {
                    (Some(street), None, _) => street,
                    (Some(street), Some(address), _) => format!("{} {}", street, address),
                    _ => String::from(""),
                };

                let (
                    selectable_item_catalog_container,
                    item_sidebar_container,
                    item_catalog_header_container,
                ) = match self.selected_buttons() {
                    (Some(street), Some(address), _) => (
                        Container::<AppMessage>::new(
                            self.selectable_item_catalog
                                .view(&street, &address)
                                .map(AppMessage::SelectableItemCatalogMessage),
                        ),
                        Container::<AppMessage>::new(
                            self.item_sidebar.view().map(AppMessage::ItemSidebarMessage),
                        ),
                        Container::<AppMessage>::new(
                            self.item_catalog_header
                                .view(&street, &address)
                                .map(AppMessage::ItemCatalogHeaderMessage),
                        ),
                    ),
                    _ => (
                        Container::new(
                            Text::new("vide")
                                .horizontal_alignment(HorizontalAlignment::Center)
                                .vertical_alignment(VerticalAlignment::Center)
                                .height(Length::Fill)
                                .width(Length::Fill),
                        ),
                        Container::new(
                            Text::new("vide")
                                .horizontal_alignment(HorizontalAlignment::Center)
                                .vertical_alignment(VerticalAlignment::Center)
                                .height(Length::Fill)
                                .width(Length::Fill),
                        ),
                        Container::new(
                            Text::new("vide")
                                .horizontal_alignment(HorizontalAlignment::Center)
                                .vertical_alignment(VerticalAlignment::Center)
                                .height(Length::Fill)
                                .width(Length::Fill),
                        ),
                    ),
                };

                let address_searchbar_container = match &self.street_searchbar.selected_button {
                    Some(id) => Container::<AppMessage>::new(
                        self.address_searchbars
                            .get_mut(id)
                            .unwrap()
                            .view()
                            .map(AppMessage::AddressSearchbarMessage),
                    ),
                    None => Container::new(
                        Text::new("vide")
                            .horizontal_alignment(HorizontalAlignment::Center)
                            .vertical_alignment(VerticalAlignment::Center)
                            .height(Length::Fill)
                            .width(Length::Fill),
                    ),
                };

                let street_searchbar_container = Container::<AppMessage>::new(
                    self.street_searchbar
                        .view()
                        .map(AppMessage::StreetSearchbarMessage),
                );

                let continue_button = Button::<AppMessage>::new(
                    &mut self.continue_button,
                    Text::new("Continuer")
                        .horizontal_alignment(HorizontalAlignment::Center)
                        .vertical_alignment(VerticalAlignment::Center),
                )
                .on_press(AppMessage::MainContinuePressedMessage);

                Container::new(
                    Container::new(
                        Row::new()
                            .push(
                                // Street searchbar
                                Container::new(street_searchbar_container)
                                    .width(Length::Units(300))
                                    .height(Length::Fill)
                                    .center_x()
                                    .center_y()
                                    .style(style::Container),
                            )
                            .push(
                                // Address searchbar
                                Container::new(address_searchbar_container)
                                    .width(Length::Units(200))
                                    .height(Length::Fill)
                                    .center_x()
                                    .center_y()
                                    .style(style::Container),
                            )
                            .push(
                                Container::new(
                                    Column::new()
                                        .push(
                                            Row::new()
                                                .push(
                                                    Text::new(address_string)
                                                        .size(30)
                                                        .width(Length::Units(200))
                                                        .height(Length::Fill)
                                                        .horizontal_alignment(
                                                            HorizontalAlignment::Center,
                                                        )
                                                        .vertical_alignment(
                                                            VerticalAlignment::Center,
                                                        ),
                                                )
                                                .push(
                                                    Row::new()
                                                        .push(
                                                            item_catalog_header_container
                                                                .padding(10)
                                                                .width(Length::Fill)
                                                                .height(Length::Fill)
                                                                .align_x(Align::Center)
                                                                .align_y(Align::Center),
                                                        )
                                                        .push(continue_button),
                                                )
                                                .width(Length::Fill)
                                                .height(Length::Units(60)),
                                        )
                                        .push(
                                            Container::new(
                                                Row::new()
                                                    .push(
                                                        item_sidebar_container
                                                            .height(Length::Fill)
                                                            .width(Length::Units(300))
                                                            .style(style::Container),
                                                    )
                                                    .push(
                                                        selectable_item_catalog_container
                                                            .height(Length::Fill)
                                                            .width(Length::Fill)
                                                            .style(style::Container),
                                                    ),
                                            )
                                            .width(Length::Fill)
                                            .height(Length::Fill)
                                            .style(style::Container),
                                        ),
                                )
                                .width(Length::Fill)
                                .height(Length::Fill)
                                .center_x()
                                .center_y()
                                .style(style::Container),
                            )
                            .spacing(30),
                    )
                    .height(Length::Fill)
                    .center_y()
                    .style(style::Container),
                )
                .padding(30)
                .height(Length::Fill)
                .width(Length::Fill)
                .center_y()
                .style(style::Container)
                .into()
            }
            AppState::End => self.end_screen.view().map(AppMessage::EndScreenMessage),
        }
    }
}

fn main() -> iced::Result {
    App::run(Settings::default())
}
