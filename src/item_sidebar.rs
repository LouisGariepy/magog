use iced::{
    button, pick_list, scrollable, text_input, Align, Button, Column, Container, Element,
    HorizontalAlignment, Length, PickList, Row, Sandbox, Scrollable, Text, TextInput,
    VerticalAlignment,
};

use crate::utils::{ButtonInfo, OrderedDictionary};
use crate::{style, utils};
use indexmap::map::IndexMap;
use serde::{Serialize, Serializer};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ItemCondition {
    Acceptable,
    Reparable,
    Irreparable,
}

impl ItemCondition {
    const ALL: [ItemCondition; 3] = [
        ItemCondition::Acceptable,
        ItemCondition::Reparable,
        ItemCondition::Irreparable,
    ];
}

impl Default for ItemCondition {
    fn default() -> Self {
        ItemCondition::Irreparable
    }
}

impl std::fmt::Display for ItemCondition {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                ItemCondition::Reparable => "Réparable",
                ItemCondition::Acceptable => "Acceptable",
                ItemCondition::Irreparable => "Irréparable",
            }
        )
    }
}

impl Serialize for ItemCondition {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}

#[derive(Default)]
pub(crate) struct ItemSidebar {
    pub buttons: OrderedDictionary<ButtonInfo>,
    input_state: text_input::State,
    pub(crate) input_value: String,
    nb_input_state: text_input::State,
    pub(crate) nb_input_value: String,
    pub selected_button: Option<String>,
    scrollable_state: scrollable::State,
    item_condition_list: pick_list::State<ItemCondition>,
    pub(crate) selected_item_condition: ItemCondition,
    confirm_button: button::State,
}

#[derive(Debug, Clone)]
pub enum ItemSidebarMessage {
    InputChanged(String),
    NbInputChanged(String),
    ItemPressed(String),
    ItemSubmit,
    ItemConditionSelected(ItemCondition),
    ConfirmButtonPressed,
}

impl ItemSidebar {
    pub(crate) fn add_item(&mut self, s: String) {
        if !self.buttons.contains_key(&s) {
            self.buttons.insert(
                &s,
                ButtonInfo {
                    state: Default::default(),
                    text: s.clone(),
                },
            );
            self.input_value = String::from("");
        }
    }

    pub(crate) fn reset(&mut self) {
        self.selected_button = Default::default();
        self.nb_input_value = Default::default();
        self.input_value = Default::default();
        self.selected_item_condition = Default::default();
    }
}

impl Sandbox for ItemSidebar {
    type Message = ItemSidebarMessage;

    fn new() -> Self {
        Self::default()
    }

    fn title(&self) -> String {
        String::from("Item Sidebar")
    }

    fn update(&mut self, message: Self::Message) {
        match message {
            ItemSidebarMessage::InputChanged(value) => {
                self.buttons.sort_match(&value);
                self.input_value = value;
            }
            ItemSidebarMessage::ItemPressed(item) => self.selected_button = Some(item),
            ItemSidebarMessage::ItemSubmit => {
                if let Some(key_value) = self.buttons.first() {
                    self.selected_button = Some(key_value.0.clone())
                }
            }
            ItemSidebarMessage::NbInputChanged(value) => {
                if value.is_empty() {
                    self.nb_input_value = value.clone()
                }
                if let Ok(nb) = value.parse::<usize>() {
                    self.nb_input_value = value;
                }
            }
            ItemSidebarMessage::ItemConditionSelected(item_condition) => {
                self.selected_item_condition = item_condition;
            }
            ItemSidebarMessage::ConfirmButtonPressed => {}
        }
    }

    fn view(&mut self) -> Element<ItemSidebarMessage> {
        let item_condition_pick_list = PickList::new(
            &mut self.item_condition_list,
            &ItemCondition::ALL[..],
            Some(self.selected_item_condition),
            ItemSidebarMessage::ItemConditionSelected,
        );

        let mut confirm_button = Button::new(
            &mut self.confirm_button,
            Text::new("Confirmer")
                .horizontal_alignment(HorizontalAlignment::Center)
                .vertical_alignment(VerticalAlignment::Center)
                .width(Length::Fill),
        );

        if !self.nb_input_value.is_empty() && self.selected_button.is_some() {
            confirm_button = confirm_button.on_press(ItemSidebarMessage::ConfirmButtonPressed);
        }

        let mut scrollable_list =
            Column::<ItemSidebarMessage>::new().width(Length::FillPortion(95));

        // List of selectable buttons
        for button in &mut self.buttons.iter_values_mut() {
            let is_selected = match &self.selected_button {
                Some(selected) => selected == &button.text,
                None => false,
            };

            scrollable_list = scrollable_list.push(
                Button::new(
                    &mut button.state,
                    Text::new(button.text.clone())
                        .horizontal_alignment(HorizontalAlignment::Center)
                        .width(Length::Fill),
                )
                .style(style::Button { is_selected })
                .width(Length::Fill)
                .on_press(ItemSidebarMessage::ItemPressed(button.text.clone())),
            );
        }

        // Content which wraps the textbox, scrollable list and remove button widgets
        Container::new(
            Column::new()
                .push(
                    // Text input
                    TextInput::<ItemSidebarMessage>::new(
                        &mut self.nb_input_state,
                        "Quantité...",
                        &self.nb_input_value,
                        ItemSidebarMessage::NbInputChanged,
                    )
                    .padding(10)
                    .width(Length::Fill),
                )
                .push(
                    // Text input
                    TextInput::<ItemSidebarMessage>::new(
                        &mut self.input_state,
                        "Item...",
                        &self.input_value,
                        ItemSidebarMessage::InputChanged,
                    )
                    .padding(10)
                    .width(Length::Fill)
                    .on_submit(ItemSidebarMessage::ItemSubmit),
                )
                .push(
                    Container::new(
                        // Scrollable list
                        Scrollable::<ItemSidebarMessage>::new(&mut self.scrollable_state)
                            .push(Row::<ItemSidebarMessage>::new().push(scrollable_list).push(
                                Column::<ItemSidebarMessage>::new().width(Length::FillPortion(5)),
                            ))
                            .align_items(Align::Center)
                            .width(Length::Fill),
                    )
                    .width(Length::Fill)
                    .height(Length::Fill),
                )
                .push(item_condition_pick_list)
                .push(confirm_button.width(Length::Fill))
                .align_items(Align::Center)
                .padding(20)
                .spacing(20),
        )
        .style(style::Container)
        .into()
    }
}
