use crate::item_sidebar::ItemCondition;
use crate::utils::Dictionary;
use crate::{item_sidebar, style};
use iced::{
    button, scrollable, Align, Button, Column, Container, Element, HorizontalAlignment, Length,
    Row, Scrollable, Text, VerticalAlignment,
};

#[derive(Default)]
pub struct ItemRow {
    pub quantity: usize,
    pub name: String,
    pub condition: ItemCondition,
}

#[derive(Default)]
pub struct ItemRowButton {
    pub item_row: ItemRow,
    button_state: button::State,
}

#[derive(Default)]
pub(crate) struct SelectableItemCatalog {
    scrollable_state: scrollable::State,
    add_button_state: button::State,
    remove_button_state: button::State,
    pub(crate) item_list: Dictionary<Vec<ItemRowButton>>,
    pub selected_button: Option<usize>,
}

#[derive(Debug, Clone)]
pub enum SelectableItemCatalogMessage {
    AddButtonPressed,
    RemoveButtonPressed,
    ItemPressed(usize, String, String, ItemCondition),
}

impl SelectableItemCatalog {
    pub(crate) fn add_item(
        &mut self,
        street_id: String,
        address_id: String,
        item_name: String,
        quantity: usize,
        item_state: item_sidebar::ItemCondition,
    ) {
        match self.selected_button {
            Some(row_id) => {
                self.item_list
                    .get_mut(&format!("{}{}", street_id, address_id))
                    .unwrap()[row_id] = ItemRowButton {
                    item_row: ItemRow {
                        quantity,
                        name: item_name,
                        condition: item_state,
                    },
                    button_state: button::State::new(),
                }
            }
            None => {
                self.item_list
                    .get_mut(&format!("{}{}", street_id, address_id))
                    .unwrap()
                    .push(ItemRowButton {
                        item_row: ItemRow {
                            quantity,
                            name: item_name,
                            condition: item_state,
                        },
                        button_state: button::State::new(),
                    });
            }
        }
    }

    pub(crate) fn remove_item(&mut self, street_id: String, address_id: String) {
        self.item_list
            .get_mut(&format!("{}{}", street_id, address_id))
            .unwrap()
            .remove(self.selected_button.unwrap());

        self.selected_button = None;
    }

    fn new() -> Self {
        Self::default()
    }

    pub(crate) fn update(&mut self, message: SelectableItemCatalogMessage) {
        match message {
            SelectableItemCatalogMessage::AddButtonPressed => self.selected_button = None,
            SelectableItemCatalogMessage::RemoveButtonPressed => {}
            SelectableItemCatalogMessage::ItemPressed(row_id, item_name, quantity, condition) => {
                self.selected_button = Some(row_id)
            }
        }
    }

    pub(crate) fn view(
        &mut self,
        street_id: &str,
        address_id: &str,
    ) -> Element<SelectableItemCatalogMessage> {
        let mut scrollable_list =
            Column::<SelectableItemCatalogMessage>::new().width(Length::FillPortion(97));

        // List of selectable buttons
        for (id, button) in (&mut self
            .item_list
            .get_mut(&format!("{}{}", street_id, address_id))
            .unwrap()
            .iter_mut())
            .enumerate()
        {
            let is_selected = match self.selected_button {
                Some(selected) => selected == id,
                None => false,
            };

            scrollable_list = scrollable_list.push(
                Button::new(
                    &mut button.button_state,
                    Row::new()
                        .push(
                            Text::new(button.item_row.quantity.to_string())
                                .horizontal_alignment(HorizontalAlignment::Center)
                                .width(Length::Fill),
                        )
                        .push(
                            Text::new(button.item_row.name.clone())
                                .horizontal_alignment(HorizontalAlignment::Center)
                                .width(Length::Fill),
                        )
                        .push(
                            Text::new(button.item_row.condition.to_string())
                                .horizontal_alignment(HorizontalAlignment::Center)
                                .width(Length::Fill),
                        )
                        .align_items(Align::Center),
                )
                .style(style::Button { is_selected })
                .width(Length::Fill)
                .on_press(SelectableItemCatalogMessage::ItemPressed(
                    id,
                    button.item_row.name.clone(),
                    button.item_row.quantity.to_string(),
                    button.item_row.condition,
                )),
            );
        }

        let mut add_button = Button::new(
            &mut self.add_button_state,
            Text::new("Ajouter")
                .horizontal_alignment(HorizontalAlignment::Center)
                .vertical_alignment(VerticalAlignment::Center)
                .width(Length::Fill),
        );

        let mut remove_button = Button::new(
            &mut self.remove_button_state,
            Text::new("Supprimer")
                .horizontal_alignment(HorizontalAlignment::Center)
                .vertical_alignment(VerticalAlignment::Center)
                .width(Length::Fill),
        );

        if let Some(id) = self.selected_button {
            add_button = add_button.on_press(SelectableItemCatalogMessage::AddButtonPressed);
            remove_button =
                remove_button.on_press(SelectableItemCatalogMessage::RemoveButtonPressed);
        }

        Container::new(
            Column::new()
                .push(Row::new().push(add_button).push(remove_button))
                .push(
                    // Scrollable list
                    Scrollable::<SelectableItemCatalogMessage>::new(&mut self.scrollable_state)
                        .push(
                            Row::<SelectableItemCatalogMessage>::new()
                                .push(scrollable_list)
                                .push(
                                    Column::<SelectableItemCatalogMessage>::new()
                                        .width(Length::FillPortion(3)),
                                ),
                        )
                        .align_items(Align::Center)
                        .width(Length::Fill)
                        .height(Length::Fill),
                ),
        )
        .into()
    }
}
