use iced::{
    button, scrollable, text_input, Align, Button, Column, Container, Element, HorizontalAlignment,
    Length, Row, Sandbox, Scrollable, Svg, Text, TextInput, VerticalAlignment,
};

use crate::street_searchbar::StreetSearchbarMessage::ItemStatusChanged;
use crate::style;
use crate::utils::{ButtonInfo, OrderedDictionary};
use serde::ser::SerializeStruct;
use serde::{Serialize, Serializer};
use std::fmt::{Display, Formatter};

#[derive(Clone)]
pub enum StreetStatus {
    Empty,
    InProgress,
    Complete,
}

impl Serialize for StreetStatus {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        match self {
            StreetStatus::Empty => serializer.serialize_str("vide"),
            StreetStatus::InProgress => serializer.serialize_str("en cours"),
            StreetStatus::Complete => serializer.serialize_str("terminé"),
        }
    }
}

impl Default for StreetStatus {
    fn default() -> Self {
        StreetStatus::Empty
    }
}

#[derive(Default)]
pub struct SelectableStatusButton {
    pub status: StreetStatus,
    pub status_button_state: button::State,
    pub selectable_button: ButtonInfo,
}

impl Serialize for SelectableStatusButton {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        let mut struc = serializer.serialize_struct("street", 2)?;
        struc.serialize_field("status", &self.status)?;
        struc.serialize_field("name", &self.selectable_button)?;
        struc.end()
    }
}

impl Display for SelectableStatusButton {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "{}", self.selectable_button.text)
    }
}

impl SelectableStatusButton {
    fn next_status(&mut self) {
        self.status = match self.status {
            StreetStatus::Empty => StreetStatus::InProgress,
            StreetStatus::InProgress => StreetStatus::Complete,
            StreetStatus::Complete => StreetStatus::InProgress,
        };
    }
}

impl Default for Icon {
    fn default() -> Self {
        Icon {
            check: Svg::from_path(format!(
                "{}/resources/check.svg",
                env!("CARGO_MANIFEST_DIR")
            ))
            .height(Length::Units(20))
            .width(Length::Units(20)),
            empty: Svg::from_path(format!(
                "{}/resources/empty.svg",
                env!("CARGO_MANIFEST_DIR")
            ))
            .height(Length::Units(20))
            .width(Length::Units(20)),
            half: Svg::from_path(format!("{}/resources/half.svg", env!("CARGO_MANIFEST_DIR")))
                .height(Length::Units(20))
                .width(Length::Units(20)),
        }
    }
}

struct Icon {
    check: Svg,
    empty: Svg,
    half: Svg,
}

#[derive(Default)]
pub(crate) struct StreetSearchbar {
    pub(crate) buttons: OrderedDictionary<SelectableStatusButton>,
    icons: Icon,
    input_state: text_input::State,
    pub(crate) input_value: String,
    remove_button_state: button::State,
    pub selected_button: Option<String>,
    scrollable_state: scrollable::State,
}

impl StreetSearchbar {
    pub(crate) fn add_item(&mut self, item: String) {
        if !self.buttons.contains_key(&item) {
            self.buttons.insert(
                &item,
                SelectableStatusButton {
                    status: StreetStatus::Empty,
                    status_button_state: Default::default(),
                    selectable_button: ButtonInfo {
                        state: Default::default(),
                        text: item.clone(),
                    },
                },
            );
            self.selected_button = Some(item);
        }
    }
}

#[derive(Debug, Clone)]
pub enum StreetSearchbarMessage {
    InputChanged(String),
    ItemPressed(String),
    ItemSubmit,
    ItemRemove,
    ItemStatusChanged(String),
}

impl StreetSearchbar {
    pub(crate) fn update(&mut self, message: StreetSearchbarMessage) {
        match message {
            StreetSearchbarMessage::InputChanged(value) => {
                self.input_value = value;
                self.buttons.sort_match(&self.input_value);
            }
            StreetSearchbarMessage::ItemPressed(street) => self.selected_button = Some(street),
            StreetSearchbarMessage::ItemSubmit => {
                if !self.buttons.contains_key(&self.input_value) {
                    self.buttons.insert(
                        &self.input_value,
                        SelectableStatusButton {
                            status: StreetStatus::Empty,
                            status_button_state: Default::default(),
                            selectable_button: ButtonInfo {
                                state: Default::default(),
                                text: self.input_value.clone(),
                            },
                        },
                    );

                    self.selected_button = Some(self.input_value.clone());
                    self.input_value = String::from("");
                }
            }
            StreetSearchbarMessage::ItemRemove => {
                if let Some(selected) = &self.selected_button {
                    self.buttons.remove(selected);
                    self.selected_button = None;
                }
            }
            StreetSearchbarMessage::ItemStatusChanged(button) => {
                self.buttons
                    .get_mut(&button)
                    .expect("expected id in buttons map")
                    .next_status();
            }
        }
    }

    pub(crate) fn view(&mut self) -> Element<StreetSearchbarMessage> {
        let mut scrollable_list =
            Column::<StreetSearchbarMessage>::new().width(Length::FillPortion(95));

        // List of selectable buttons
        for button in &mut self.buttons.iter_values_mut() {
            let is_selected = match &self.selected_button {
                Some(selected) => selected == &button.selectable_button.text,
                None => false,
            };

            let svg = match button.status {
                StreetStatus::Empty => self.icons.empty.clone(),
                StreetStatus::InProgress => self.icons.half.clone(),
                StreetStatus::Complete => self.icons.check.clone(),
            };

            let street_button = Button::new(
                &mut button.selectable_button.state,
                Text::new(button.selectable_button.text.clone())
                    .horizontal_alignment(HorizontalAlignment::Center)
                    .vertical_alignment(VerticalAlignment::Center),
            )
            .width(Length::Fill);

            scrollable_list = scrollable_list.push(
                Row::new()
                    .push(
                        Button::new(&mut button.status_button_state, svg)
                            .on_press(ItemStatusChanged(button.selectable_button.text.clone())),
                    )
                    .push(
                        street_button
                            .width(Length::Fill)
                            .padding(10)
                            .on_press(StreetSearchbarMessage::ItemPressed(
                                button.selectable_button.text.clone(),
                            ))
                            .style(style::Button { is_selected }),
                    )
                    .spacing(10)
                    .align_items(Align::Center),
            )
        }

        // Content which wraps the textbox, scrollable list and remove button widgets
        Container::new(
            Column::new()
                .push(
                    // Text input
                    TextInput::<StreetSearchbarMessage>::new(
                        &mut self.input_state,
                        "Rue...",
                        &self.input_value,
                        StreetSearchbarMessage::InputChanged,
                    )
                    .padding(10)
                    .width(Length::Fill)
                    .on_submit(StreetSearchbarMessage::ItemSubmit),
                )
                .push(
                    // Scrollable list
                    Scrollable::<StreetSearchbarMessage>::new(&mut self.scrollable_state)
                        .push(
                            Row::<StreetSearchbarMessage>::new()
                                .push(scrollable_list)
                                .push(
                                    Column::<StreetSearchbarMessage>::new()
                                        .width(Length::FillPortion(5)),
                                ),
                        )
                        .align_items(Align::Center)
                        .height(Length::Fill)
                        .width(Length::Fill),
                )
                .push(
                    // Delete button
                    Button::new(
                        &mut self.remove_button_state,
                        Text::new("Retirer")
                            .horizontal_alignment(HorizontalAlignment::Center)
                            .vertical_alignment(VerticalAlignment::Center)
                            .width(Length::Fill),
                    )
                    .width(Length::Fill)
                    .height(Length::Units(30))
                    .on_press(StreetSearchbarMessage::ItemRemove)
                    .style(style::RemoveButton),
                )
                .align_items(Align::Center)
                .padding(20)
                .spacing(10),
        )
        .style(style::Container)
        .into()
    }
}
