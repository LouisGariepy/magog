use crate::item_catalog_header::{HeapSize, HousingType};
use crate::item_sidebar::ItemCondition;
use crate::start_screen::Sector;
use crate::street_searchbar::StreetStatus;
use chrono::{DateTime, Local};
use iced::{button, Button, Element, HorizontalAlignment, Length, Text, VerticalAlignment};
use indexmap::map::{Values, ValuesMut};
use indexmap::IndexMap;
use serde::ser::{SerializeMap, SerializeSeq, SerializeStruct};
use serde::{Serialize, Serializer};
use std::fmt::{Display, Formatter};
use std::time::{Duration, Instant};
use strsim::jaro_winkler;

#[derive(Default)]
pub struct ButtonInfo {
    pub(crate) state: button::State,
    pub(crate) text: String,
}

impl Display for ButtonInfo {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "{}", self.text)
    }
}

impl Serialize for ButtonInfo {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.text)
    }
}

#[derive(Serialize)]
pub struct SectorData {
    pub sector: Sector,
    pub mileage: i32,
    pub date: String,
    pub duration: String,
    pub street_data: Vec<StreetData>,
}

#[derive(Default, Serialize)]
pub struct StreetData {
    pub street_status: StreetStatus,
    pub street_name: String,
    pub address_data: Vec<AddressData>,
}

#[derive(Default, Serialize)]
pub struct AddressData {
    pub address_name: String,
    pub housing_type: HousingType,
    pub heap_size: HeapSize,
    pub heap_data: Vec<ItemData>,
}

#[derive(Default, Clone, Serialize)]
pub struct ItemData {
    pub item_name: String,
    pub quantity: usize,
    pub condition: ItemCondition,
}

#[derive(Default)]
pub struct OrderedDictionary<V: Serialize + Display> {
    map: IndexMap<String, V>,
}

impl<V: Serialize + Display> OrderedDictionary<V> {
    pub fn first(&mut self) -> Option<(&String, &V)> {
        self.map.first()
    }

    pub(crate) fn insert(&mut self, key: &str, item: V) -> Option<V> {
        self.map.insert(key.to_string(), item)
    }

    pub(crate) fn remove(&mut self, key: &str) -> Option<V> {
        self.map.remove(key)
    }

    pub(crate) fn contains_key(&self, key: &str) -> bool {
        self.map.contains_key(key)
    }

    pub(crate) fn get(&self, key: &str) -> Option<&V> {
        self.map.get(key)
    }

    pub(crate) fn get_mut(&mut self, key: &str) -> Option<&mut V> {
        self.map.get_mut(key)
    }

    fn get_at_index(&self, index: usize) -> Option<(&String, &V)> {
        self.map.get_index(index)
    }

    fn get_at_index_mut(&mut self, index: usize) -> Option<(&mut String, &mut V)> {
        self.map.get_index_mut(index)
    }

    pub(crate) fn iter_values(&self) -> Values<String, V> {
        self.map.values()
    }

    pub(crate) fn iter_values_mut(&mut self) -> ValuesMut<String, V> {
        self.map.values_mut()
    }

    pub(crate) fn sort_match(&mut self, pattern: &str) {
        self.map.sort_by(|_, a: &V, _, b: &V| {
            jaro_winkler(pattern, &b.to_string())
                .partial_cmp(&jaro_winkler(pattern, &a.to_string()))
                .unwrap()
        });
    }
}

impl<V> Serialize for OrderedDictionary<V>
where
    V: Serialize + Display,
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut map = serializer.serialize_seq(Some(self.map.len()))?;
        for item in self.map.values() {
            map.serialize_element(item)?;
        }
        map.end()
    }
}

#[derive(Default)]
pub struct Dictionary<V> {
    map: IndexMap<String, V>,
}

impl<V> Dictionary<V> {
    pub fn first(&mut self) -> Option<(&String, &V)> {
        self.map.first()
    }

    pub(crate) fn insert(&mut self, key: &str, item: V) -> Option<V> {
        self.map.insert(key.to_string(), item)
    }

    pub(crate) fn remove(&mut self, key: &str) -> Option<V> {
        self.map.remove(key)
    }

    pub(crate) fn contains_key(&self, key: &str) -> bool {
        self.map.contains_key(key)
    }

    pub(crate) fn get(&self, key: &str) -> Option<&V> {
        self.map.get(key)
    }

    pub(crate) fn get_mut(&mut self, key: &str) -> Option<&mut V> {
        self.map.get_mut(key)
    }

    fn get_at_index(&self, index: usize) -> Option<(&String, &V)> {
        self.map.get_index(index)
    }

    fn get_at_index_mut(&mut self, index: usize) -> Option<(&mut String, &mut V)> {
        self.map.get_index_mut(index)
    }

    fn iter_values(&self) -> Values<String, V> {
        self.map.values()
    }

    fn iter_values_mut(&mut self) -> ValuesMut<String, V> {
        self.map.values_mut()
    }
}
