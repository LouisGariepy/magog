use iced::{
    button, scrollable, text_input, Align, Button, Column, Container, Element, HorizontalAlignment,
    Length, Row, Sandbox, Scrollable, Text, TextInput, VerticalAlignment,
};

use crate::style;
use crate::utils::{ButtonInfo, OrderedDictionary};

#[derive(Default)]
pub(crate) struct AddressSearchbar {
    pub buttons: OrderedDictionary<ButtonInfo>,
    input_state: text_input::State,
    pub(crate) input_value: String,
    remove_button_state: button::State,
    pub selected_button: Option<String>,
    scrollable_state: scrollable::State,
}

#[derive(Debug, Clone)]
pub enum AddressSearchbarMessage {
    InputChanged(String),
    ItemPressed(String),
    ItemSubmit,
    ItemRemove,
}

impl AddressSearchbar {
    pub(crate) fn update(&mut self, message: AddressSearchbarMessage) {
        match message {
            AddressSearchbarMessage::InputChanged(value) => {
                self.buttons.sort_match(&value);
                self.input_value = value;
            }
            AddressSearchbarMessage::ItemPressed(button) => self.selected_button = Some(button),
            AddressSearchbarMessage::ItemSubmit => {
                self.buttons.insert(
                    &self.input_value,
                    ButtonInfo {
                        state: Default::default(),
                        text: self.input_value.clone(),
                    },
                );

                self.selected_button = Some(self.input_value.clone());
                self.input_value = String::from("");
            }
            AddressSearchbarMessage::ItemRemove => {
                if let Some(selected) = &self.selected_button {
                    self.buttons.remove(selected);
                    self.selected_button = None;
                }
            }
        }
    }

    pub(crate) fn view(&mut self) -> Element<AddressSearchbarMessage> {
        let mut scrollable_list =
            Column::<AddressSearchbarMessage>::new().width(Length::FillPortion(95));

        // List of selectable buttons
        for button in &mut self.buttons.iter_values_mut() {
            let is_selected = match &self.selected_button {
                Some(selected) => selected == &button.text,
                None => false,
            };

            let address_button = Button::new(
                &mut button.state,
                Text::new(button.text.clone())
                    .horizontal_alignment(HorizontalAlignment::Center)
                    .vertical_alignment(VerticalAlignment::Center)
                    .width(Length::Fill),
            )
            .width(Length::Fill)
            .height(Length::Units(30));

            scrollable_list = scrollable_list.push(
                address_button
                    .style(style::Button { is_selected })
                    .on_press(AddressSearchbarMessage::ItemPressed(button.text.clone())),
            );
        }

        let delete_button = Button::new(
            &mut self.remove_button_state,
            Text::new(String::from("Retier"))
                .horizontal_alignment(HorizontalAlignment::Center)
                .vertical_alignment(VerticalAlignment::Center)
                .width(Length::Fill),
        )
        .width(Length::Fill)
        .height(Length::Units(30));

        // Content which wraps the textbox, scrollable list and remove button widgets
        Container::new(
            Column::new()
                .push(
                    // Text input
                    TextInput::<AddressSearchbarMessage>::new(
                        &mut self.input_state,
                        "Adresse...",
                        &self.input_value,
                        AddressSearchbarMessage::InputChanged,
                    )
                    .padding(10)
                    .width(Length::Fill)
                    .on_submit(AddressSearchbarMessage::ItemSubmit),
                )
                .push(
                    // Scrollable list
                    Scrollable::<AddressSearchbarMessage>::new(&mut self.scrollable_state)
                        .push(
                            Row::<AddressSearchbarMessage>::new()
                                .push(scrollable_list)
                                .push(
                                    Column::<AddressSearchbarMessage>::new()
                                        .width(Length::FillPortion(5)),
                                ),
                        )
                        .align_items(Align::Center)
                        .height(Length::Fill)
                        .width(Length::Fill),
                )
                .push(
                    // Delete button
                    delete_button
                        .on_press(AddressSearchbarMessage::ItemRemove)
                        .style(style::RemoveButton),
                )
                .align_items(Align::Center)
                .padding(20)
                .spacing(10),
        )
        .style(style::Container)
        .into()
    }
}
