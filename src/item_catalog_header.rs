use iced::{
    button, pick_list, scrollable, text_input, Align, Button, Column, Container, Element,
    HorizontalAlignment, Length, PickList, Row, Sandbox, Scrollable, Text, TextInput,
    VerticalAlignment,
};

use crate::utils::Dictionary;
use serde::{Serialize, Serializer};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum HeapSize {
    Small,
    Medium,
    Large,
    VeryLarge,
}

impl HeapSize {
    const ALL: [HeapSize; 4] = [
        HeapSize::Small,
        HeapSize::Medium,
        HeapSize::Large,
        HeapSize::VeryLarge,
    ];
}

impl Default for HeapSize {
    fn default() -> Self {
        HeapSize::Medium
    }
}

impl std::fmt::Display for HeapSize {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                HeapSize::Small => "Petit (< 1 m³)",
                HeapSize::Medium => "Moyen (< 2 m³)",
                HeapSize::Large => "Grand (< 3 m³)",
                HeapSize::VeryLarge => "Très grand (≥ 3 m³)",
            }
        )
    }
}

impl Serialize for HeapSize {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum HousingType {
    SingleFamily,
    Duplex,
    Condo,
    MultiFamily5,
    MultiFamily6,
}

impl HousingType {
    const ALL: [HousingType; 5] = [
        HousingType::SingleFamily,
        HousingType::Duplex,
        HousingType::Condo,
        HousingType::MultiFamily5,
        HousingType::MultiFamily6,
    ];
}

impl Default for HousingType {
    fn default() -> Self {
        HousingType::SingleFamily
    }
}

impl std::fmt::Display for HousingType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                HousingType::SingleFamily => "Unifamilial",
                HousingType::Duplex => "Duplex",
                HousingType::Condo => "Condo",
                HousingType::MultiFamily5 => "Multilogement 5 et moins",
                HousingType::MultiFamily6 => "Multilogeemnt 6 et plus",
            }
        )
    }
}

impl Serialize for HousingType {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}

#[derive(Default)]
pub(crate) struct HeapInfo {
    heapsize_list: pick_list::State<HeapSize>,
    pub selected_heapsize: HeapSize,
    housing_list: pick_list::State<HousingType>,
    pub selected_housing: HousingType,
}

impl std::fmt::Display for HeapInfo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}{}", self.selected_housing, self.selected_heapsize,)
    }
}

#[derive(Default)]
pub(crate) struct ItemCatalogHeader {
    pub(crate) heaps: Dictionary<HeapInfo>,
}

#[derive(Debug, Clone)]
pub enum ItemCatalogHeaderMessage {
    HousingTypeSelected(HousingType),
    HeapSizeSelected(HeapSize),
}

impl ItemCatalogHeader {
    pub(crate) fn update(&mut self, message: ItemCatalogHeaderMessage, street: &str, adress: &str) {
        let heap = &mut self
            .heaps
            .get_mut(&format!("{}{}", street, adress))
            .unwrap();
        match message {
            ItemCatalogHeaderMessage::HousingTypeSelected(housingtype) => {
                heap.selected_housing = housingtype;
            }
            ItemCatalogHeaderMessage::HeapSizeSelected(heapsize) => {
                heap.selected_heapsize = heapsize;
            }
        }
    }

    pub(crate) fn view(
        &mut self,
        street: &str,
        address: &str,
    ) -> Element<ItemCatalogHeaderMessage> {
        let heap = self
            .heaps
            .get_mut(&format!("{}{}", street, address))
            .unwrap();

        let housing_pick_list = PickList::new(
            &mut heap.housing_list,
            &HousingType::ALL[..],
            Some(heap.selected_housing),
            ItemCatalogHeaderMessage::HousingTypeSelected,
        );

        let heapsize_pick_list = PickList::new(
            &mut heap.heapsize_list,
            &HeapSize::ALL[..],
            Some(heap.selected_heapsize),
            ItemCatalogHeaderMessage::HeapSizeSelected,
        );

        Container::new(
            Row::new()
                .push(housing_pick_list)
                .push(heapsize_pick_list)
                .spacing(30),
        )
        .into()
    }
}
